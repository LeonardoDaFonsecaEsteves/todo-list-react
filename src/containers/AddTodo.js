import React from "react";
import { Button, Container, FormControl, InputGroup } from "react-bootstrap";
import { connect } from "react-redux";
import { addTodo } from "../actions";

const AddTodo = ({ dispatch }) => {
  let [input, setInputs] = React.useState("");

  const handleChange = (value) => {
    setInputs(value);
  };

  const handleSubmit = () => {
    if (input) {
      dispatch(addTodo(input));
      setInputs("");
    } else {
      return;
    }
  };

  return (
    <Container>
      <InputGroup className="mb-3">
        <FormControl
          onChange={(e) => handleChange(e.target.value)}
          placeholder="todo"
          aria-label="todo"
          aria-describedby="basic-addon2"
          value={input}
        />
        <InputGroup.Append>
          <Button onClick={() => handleSubmit()} variant="outline-secondary">
            Ajouter
          </Button>
        </InputGroup.Append>
      </InputGroup>
    </Container>
  );
};

export default connect()(AddTodo);
