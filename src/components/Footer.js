import React from "react";
import FilterLink from "../containers/FilterLink";
import { VisibilityFilters } from "../actions";
import { Container, InputGroup } from "react-bootstrap";

const Footer = () => (
  <Container>
    <InputGroup>
      <InputGroup.Prepend>
        <InputGroup.Text>Filtre: </InputGroup.Text>
      </InputGroup.Prepend>
      <FilterLink filter={VisibilityFilters.SHOW_ALL}>
        Toutes les todos
      </FilterLink>
      <FilterLink filter={VisibilityFilters.SHOW_ACTIVE}>
        Les todo active
      </FilterLink>
      <FilterLink filter={VisibilityFilters.SHOW_COMPLETED}>
        Les todo complete
      </FilterLink>
    </InputGroup>
  </Container>
);

export default Footer;
