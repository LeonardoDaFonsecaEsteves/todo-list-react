import React from "react";
import Footer from "./Footer";
import AddTodo from "../containers/AddTodo";
import VisibleTodoList from "../containers/VisibleTodoList";
import { Container, Jumbotron } from "react-bootstrap";

const App = () => (
  <Container>
    <Jumbotron>
      <h1>TODO list</h1>
    </Jumbotron>
    <AddTodo />
    <div
      style={{
        minHeight: "50vh",
        maxHeight: "50vh",
        width: "100%",
        overflow: "auto",
      }}
    >
      <VisibleTodoList />
    </div>
    <div
      style={{
        margin: "15px",
        width: "100%",
      }}
    >
      <Footer />
    </div>
  </Container>
);

export default App;
